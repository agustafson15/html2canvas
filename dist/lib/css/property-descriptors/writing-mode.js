"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.writingMode = exports.WRITING_MODE = void 0;
var WRITING_MODE;
(function (WRITING_MODE) {
    WRITING_MODE["HORIZONTAL_TB"] = "horizontal-tb";
    WRITING_MODE["VERTICAL_RL"] = "vertical-rl";
    WRITING_MODE["VERTICAL_LR"] = "vertical-lr";
})(WRITING_MODE = exports.WRITING_MODE || (exports.WRITING_MODE = {}));
exports.writingMode = {
    name: 'writing-mode',
    initialValue: 'horizontal-tb',
    prefix: false,
    type: 2 /* IDENT_VALUE */,
    parse: function (_context, writingMode) {
        switch (writingMode) {
            case 'horizontal-tb':
                return WRITING_MODE.HORIZONTAL_TB;
            case 'vertical-rl':
                return WRITING_MODE.VERTICAL_RL;
            case 'vertical-lr':
                return WRITING_MODE.VERTICAL_LR;
            default:
                return WRITING_MODE.HORIZONTAL_TB;
        }
    }
};
//# sourceMappingURL=writing-mode.js.map