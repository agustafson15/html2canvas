import { IPropertyIdentValueDescriptor } from '../IPropertyDescriptor';
export declare enum WRITING_MODE {
    HORIZONTAL_TB = "horizontal-tb",
    VERTICAL_RL = "vertical-rl",
    VERTICAL_LR = "vertical-lr"
}
export declare const writingMode: IPropertyIdentValueDescriptor<WRITING_MODE>;
